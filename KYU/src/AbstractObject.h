#ifndef __AbstractObject__
#define __AbstractObject__

#include "Header.h"

class AbstractObject

{
public:
	virtual void init() = 0;
	virtual void render() = 0;
	virtual void update() = 0; 
	virtual void handleEvents() = 0;
	~AbstractObject(void) {};
};

#endif // defined (__AbstractObject__)