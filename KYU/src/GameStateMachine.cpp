#include "GameStateMachine.h"
#include "PlayState.h"
#include "MenuState.h"

void GameStateMachine::pushState(AbstractGameState* pState)
{
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}

void GameStateMachine::popState()
{
	if (!m_gameStates.empty())
	{
		if(m_gameStates.back()->onExit())
		{
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
}

void GameStateMachine::update()
{
	if(!m_gameStates.empty())
	{
		m_gameStates.back()->update();
	}
}

void GameStateMachine::render()
{
	if(!m_gameStates.empty())
	{
		m_gameStates.back()->render();
	}
}

void GameStateMachine::handleEvents()
{
	if(!m_gameStates.empty())
	{
		m_gameStates.back()->handleEvents();
	}
}

void GameStateMachine::clean()
{
	if (!m_gameStates.empty())
		for (unsigned int i=0; i!= m_gameStates.size(); i++)
			{
				delete m_gameStates[i];
			}
}