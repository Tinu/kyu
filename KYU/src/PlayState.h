#ifndef __PlayState__
#define __PlayState__

#include "AbstractGameState.h"
#include "AbstractObject.h"

class PlayState :
	public AbstractGameState
{
private:

	std::vector<AbstractObject*> m_vobject3D;
	static const std::string s_menuID;
	
public:
	
	PlayState() {}
	~PlayState() {}
	
	void update();
	void render();
	void handleEvents();

	bool onEnter();
	bool onExit();

	void clean();

	virtual std::string getStateID() const { return s_menuID; }
};

#endif // defined (__PlayState__)