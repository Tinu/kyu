#ifndef __GameStateMachine__
#define __GameStateMachine__

#include "AbstractGameState.h"
// this class manages all the game states
// it stores them in a vector and uses only the last item
// from the vector; all operations like update, render,
// handle events, etc. are performed on the .back() of the vector

#define INTRO 0
#define MENU 1
#define PLAY 2
#define PAUSE 3
#define GAMEOVER 4

class GameStateMachine
{
private:

	std::vector<AbstractGameState*> m_gameStates;

public:
	
	GameStateMachine(void) {}
	~GameStateMachine(void) { clean(); }

	void pushState(AbstractGameState* pState);		// will add a state without removing the previous state
	void popState();								// will remove the current state without adding another

	void update();
	void render();
	void handleEvents();

	void clean();
};

#endif // defined (__GameStateMachine__)