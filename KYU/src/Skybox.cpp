#include "Skybox.h"

void Skybox::init()
{
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	
	glGenBuffers(5, &m_VBO[0]);
	glBindVertexArray(0);

	load("data/texture/skybox/");
	createSkyboxCube();
}

void Skybox::load(const std::string& filename)
{
	char buffer[128];
	for (int i = 0; i < 5; i++) 
	{
		std::string temp = _itoa(i,buffer,10);
		std::string path = filename+"texture"+temp+".bmp";
		m_textures[i] = LOADIMAGE((char*) path.c_str());

		glBindTexture(GL_TEXTURE_2D, m_textures[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
}

void Skybox::createSkyboxCube()
{
	// pushes all vertices to GPU for each side
	glBindVertexArray(m_VAO);
	
	GLfloat front[]  = {
	   -500, -250,	500.0, 0.0, 1.0,
	   -500,  250,	500.0, 0.0, 0.0,
		500, -250,	500.0, 1.0, 1.0,
		500,  250,	500.0, 1.0, 0.0
	};

	GLfloat back[]  = {
	   -500, -250,	-500.0, 1.0, 1.0,
	   -500,  250,	-500.0, 1.0, 0.0,
		500, -250,	-500.0, 0.0, 1.0,
		500,  250,	-500.0, 0.0, 0.0
	};

	GLfloat left[]  = {
	   -500, -250,	-500.0, 0.0, 1.0,
	   -500,  250,	-500.0, 0.0, 0.0,
	   -500, -250,	 500.0, 1.0, 1.0,
	   -500,  250,	 500.0, 1.0, 0.0
	};

	GLfloat right[]  = {
	    500, -250,	-500.0, 1.0, 1.0,
	    500,  250,	-500.0, 1.0, 0.0,
		500, -250,	 500.0, 0.0, 1.0,
		500,  250,	 500.0, 0.0, 0.0
	};

	GLfloat top[]  = {
	   -500,  250,	-500.0, 0.0, 0.0,
	   -500,  250,	 500.0, 0.0, 1.0,
		500,  250,	-500.0, 1.0, 0.0,
		500,  250,	 500.0, 1.0, 1.0
	};

	initialiseVBO(back, 1);
	initialiseVBO(front, 0);

	initialiseVBO(left, 2);
	initialiseVBO(right, 3);
	initialiseVBO(top, 4);

	glBindVertexArray(0);
}

void Skybox::initialiseVBO(GLfloat * data, int number)
	// function to allocate memory in buffer for one side
	// of the cube at each function call
{
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[number]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) *20, &data[0], GL_STATIC_DRAW);
}


void Skybox::render() 
{
	glDisable(GL_CULL_FACE);
	glBindVertexArray(m_VAO);
	
	for(int i = 0; i < 5; i++)
		// for each side of the skybox
	{
		glBindTexture(GL_TEXTURE_2D, m_textures[i]);
		// atatch the texture
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO[i]);
		
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
		glVertexAttribPointer(3, 2, GL_FLOAT, false, 20, (void*)12);		
		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		// and draw the triangles of the respective side
	}
	
	glBindVertexArray(0);
	glEnable(GL_CULL_FACE);
}