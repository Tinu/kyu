#include "Game.h"
#include "IntroState.h"
#include "PlayState.h"

Game* Game::s_pInstance = 0;

SDL_Window * Game::setupRenderingContext(SDL_GLContext &context, const char* title, int xpos, int ypos, int width, int height, bool fullscreen) {
	SDL_Window * window;

	int flags = 0;
	if (fullscreen) 
	{
		flags = SDL_WINDOW_FULLSCREEN;
	}

	if (xpos == CENTER && ypos == CENTER)
	{
		xpos = ypos = SDL_WINDOWPOS_CENTERED;
	}

    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("SDL Init failed\n"); 
	  
    // Request an OpenGL 3.0 context.
    // Not able to use SDL to choose profile (yet), should default to core profile on 3.2 or later
	// If you request a context not supported by your drivers, no OpenGL context will be created
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create a window with title, position and size
    window = SDL_CreateWindow(title, xpos, ypos, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | flags);
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Window init failed\n");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		rt3d::exitFatalError("glewInit failed, aborting.\n");
		exit (1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;
	
	return window;
}

bool Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
	m_pWindow = setupRenderingContext(m_glContext, title, xpos, ypos, width, height, fullscreen);
	
	if (m_pWindow ==0) 
	{
		rt3d::exitFatalError("Couldn`t attach context to window\n");
		return false; // something went wrong attaching the context to the window
	}

	
	m_pGameStateMachine = new GameStateMachine();

	// here set the default start state 
	// to go directly in play state comment the intro and uncomment the play

	m_pGameStateMachine->pushState(new IntroState());	
	//m_pGameStateMachine->pushState(new PlayState());	
	
	m_bRunning = true;

	return true;
}

void Game::update()
{
	TheInputHandler::Instance()->update();
	SDL_ShowCursor(false);
	m_pGameStateMachine->update();
	SDL_WarpMouseInWindow(m_pWindow,XCENTRE, YCENTRE);
}

void Game::handleEvents()
{
	m_pGameStateMachine->handleEvents();
}

void Game::render()
{	
	m_pGameStateMachine->render();
	SDL_GL_SwapWindow(m_pWindow); // swap buffers
}

void Game::quit()
{
	m_bRunning = false;
	SDL_GL_DeleteContext(m_glContext);
	SDL_DestroyWindow(m_pWindow);
    SDL_Quit();
}