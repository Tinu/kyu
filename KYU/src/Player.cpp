#include "Player.h"
#include "ModelLoader.h"
#include "Game.h"

// method to check for collision between a point and a rectangle
bool contacts (float& right, float& left, float& top, float &bottom, float& x, float &z) {

	// if is outside the box... then no collision
	if (x>right)	return false;
	if (x<left)		return false;
	if (z<bottom)	return false;
	if (z>top)		return false;
	
	return true;
}


bool Player::checkCollison()
{
	unsigned int i;
	bool contact;

	// collision checking against map edges
	if (m_vposition.z < 2.0f)	{ m_vposition.z = 2.0f; }
	if (m_vposition.z > 248.0f) { m_vposition.z = 248.0f; }
	if (m_vposition.x < 2.0f)	{ m_vposition.x = 2.0f; }
	if (m_vposition.x > 188.0f) { m_vposition.x = 188.0f;  }

	if (m_vposition.z < 130.0f) 
		// left map is from 0 to 130 on Z axis
	{
		for (i = 0; i < m_vleftMap.size()-1; i+=4) 
			// so read the coordinates of the two points, so two pairs of x,y 
			// that`s why we increase by 4 at each step
		{
			// check for collision between the rectangle described by the 2 points
			// they are already sorted from the loading function
			// and check against the current position of the player
			contact= contacts(m_vleftMap[i], m_vleftMap[i+1], m_vleftMap[i+2], m_vleftMap[i+3], m_vposition.x, m_vposition.z);

			if (contact) 
				// in case of contact check in which direction is possible to move
				// so find which direction collides and mark it as not able to move

			{
				m_bcanMove[FORWARD] = !contacts(m_vleftMap[i], m_vleftMap[i+1], m_vleftMap[i+2], m_vleftMap[i+3], 
					m_vnextStep[FORWARD].x, m_vnextStep[FORWARD].z);

				m_bcanMove[BACKWARD] = !contacts(m_vleftMap[i], m_vleftMap[i+1], m_vleftMap[i+2], m_vleftMap[i+3], 
					m_vnextStep[BACKWARD].x, m_vnextStep[BACKWARD].z);

				m_bcanMove[SIDELEFT] = !contacts(m_vleftMap[i], m_vleftMap[i+1], m_vleftMap[i+2], m_vleftMap[i+3], 
					m_vnextStep[SIDELEFT].x, m_vnextStep[SIDELEFT].z);

				m_bcanMove[SIDERIGHT] = !contacts(m_vleftMap[i], m_vleftMap[i+1], m_vleftMap[i+2], m_vleftMap[i+3], 
					m_vnextStep[SIDERIGHT].x, m_vnextStep[SIDERIGHT].z);

				return true;
			}
		} 
	}
	else
		// do the same for the rest of the map 
		// the one after z > 130
	{
		for (i = 0; i < m_vrightMap.size()-1; i+=4)
		{
			contact= contacts(m_vrightMap[i], m_vrightMap[i+1], m_vrightMap[i+2], m_vrightMap[i+3], m_vposition.x, m_vposition.z);

			if (contact) 
			{
				m_bcanMove[FORWARD] = !contacts(m_vrightMap[i], m_vrightMap[i+1], m_vrightMap[i+2], m_vrightMap[i+3], 
					m_vnextStep[FORWARD].x, m_vnextStep[FORWARD].z);

				m_bcanMove[BACKWARD] = !contacts(m_vrightMap[i], m_vrightMap[i+1], m_vrightMap[i+2], m_vrightMap[i+3],
					m_vnextStep[BACKWARD].x, m_vnextStep[BACKWARD].z);

				m_bcanMove[SIDELEFT] = !contacts(m_vrightMap[i], m_vrightMap[i+1], m_vrightMap[i+2], m_vrightMap[i+3],
					m_vnextStep[SIDELEFT].x, m_vnextStep[SIDELEFT].z);

				m_bcanMove[SIDERIGHT] = !contacts(m_vrightMap[i], m_vrightMap[i+1], m_vrightMap[i+2], m_vrightMap[i+3], 
					m_vnextStep[SIDERIGHT].x, m_vnextStep[SIDERIGHT].z);

				return true;
			}
		}
	}
	return false;
}


// moving functions for directional moving for the actual model
glm::vec3 Player::moveForward(glm::vec3 pos, GLfloat d) {
	return glm::vec3(pos.x + d*std::sin(yRotation*RADIANS), pos.y, pos.z - d*std::cos(yRotation*RADIANS));
}

glm::vec3 Player::moveRight(glm::vec3 pos, GLfloat d) {
	return glm::vec3(pos.x + d*std::cos(yRotation*RADIANS), pos.y, pos.z + d*std::sin(yRotation*RADIANS));
}

glm::vec3 Player::moveForward(glm::vec3 pos, GLfloat angle, GLfloat d, GLfloat rot) {
	// move on X
	return glm::vec3(pos.x + d*std::sin(angle*RADIANS), pos.y + rot, pos.z - d*std::cos(angle*RADIANS));
}

glm::vec3 Player::moveRight(glm::vec3 pos, GLfloat angle, GLfloat d, GLfloat rot) {
	// move on Z
	return glm::vec3(pos.x + d*std::cos(angle*RADIANS), pos.y + rot, pos.z + d*std::sin(angle*RADIANS));
}

// moving functions for input handling
void Player::moveForward()
{
	if (m_bcanMove[FORWARD])	{ m_bgoingForward = true;	m_vposition			= m_vnextStep[FORWARD]; }
	else						{ m_bgoingForward = false;	m_bcanMove[FORWARD] = true;					}
}

void Player::moveBackward()
{
	if (m_bcanMove[BACKWARD])	{ m_bgoingBackward = true;	m_vposition			= m_vnextStep[BACKWARD];} 
	else						{ m_bgoingBackward = false;	m_bcanMove[BACKWARD]= true;					}
}

void Player::moveLeft()
{
	if (m_bcanMove[SIDELEFT]) m_vposition = m_vnextStep[SIDELEFT]; 
	else m_bcanMove[SIDELEFT] = true;
}

void Player::moveRight()
{
	if (m_bcanMove[SIDERIGHT]) m_vposition = m_vnextStep[SIDERIGHT]; 
	else m_bcanMove[SIDERIGHT] = true;
}

void Player::init()
	// start joysticks
	// load the collision maps
	// load the model
	// set the default animation
	// set 3rd person camera
	// allow movement
	// set the speed, position and orientation
	// set the material
	// load sounds
{
	m_firstPersonCamera.init();
	JOY->initialiseJoysticks();

	// load the left map
	m_vleftMap = TheModelLoader::Instance()->loadObj("data/contact/leftMap.obj");
	// clear the vectors from the loader for reuse
	// so we don`t push more points from where we were
	TheModelLoader::Instance()->clear();

	// load the right map
	m_vrightMap = TheModelLoader::Instance()->loadObj("data/contact/rightMap.obj");

	// this is to load a different model
	//m_texture = LOADIMAGE("data/texture/player.bmp");
	//m_mesh = m_playerModel.ReadMD2Model("data/model/playermodel.md2");

	// this is to load yoshi model
	m_texture = LOADIMAGE("data/texture/yoshi.bmp");
	m_mesh = m_playerModel.ReadMD2Model("data/model/yoshi.md2");

	m_ivertexCount = m_playerModel.getVertDataCount();
	m_icurrentAnimation = 0;
	m_bfirstPersonCameraIsActive = false;
	
	m_bcanMove[FORWARD]		= true;
	m_bcanMove[SIDELEFT]	= true;
	m_bcanMove[SIDERIGHT]	= true;
	m_bcanMove[BACKWARD]	= true;
	m_moveSpeed				= 0.3f;
	m_bgoingForward			= false;
	m_bgoingBackward		= false;
	m_bisJumping			= false;
	m_bactivates			= false;
	yRotation				= 90.0f;
	xRotation				= 0.0f;
	zRotation				= 0.0f;
	m_lift					= 1.0f;

	m_vposition = glm::vec3(10.0f, 0.5f, 10.0f);
	
	rt3d::materialStruct tempMaterial = {
	{1.4f, 1.4f, 1.4f, 1.0f}, // ambient
	{0.8f, 0.8f, 1.0f, 1.0f}, // diffuse
	{0.8f, 0.8f, 0.8f, 1.0f}, // specular
	1.0f  // shininess
	};

	m_material = tempMaterial;

	sound_walk = LOADSAMPLE("data/samples/walk.wav");
	sound_jump = LOADSAMPLE("data/samples/jump.wav");
	sound_use =  LOADSAMPLE("data/samples/use.wav");

	m_benabled = true;
}

void Player::update()
{
	// calculate next position
	m_vnextStep[FORWARD]	= moveForward	(m_vposition, m_moveSpeed);
	m_vnextStep[BACKWARD]	= moveForward	(m_vposition, -m_moveSpeed);
	m_vnextStep[SIDELEFT]	= moveRight		(m_vposition, -m_moveSpeed);
	m_vnextStep[SIDERIGHT]	= moveRight		(m_vposition, m_moveSpeed);

	// collision checking against buildings 
	if (m_bcollisionEnabled) checkCollison();	

	// lock the camera look, it is useless to calculate for bigger values
	if (xRotation > 4.55)	xRotation = 4.55f;		// maximum up rotation
	if (xRotation < -4.55)	xRotation = -4.55f;		// maximum down rotation
	if (yRotation > 360)	yRotation -= 360.0f;	// for left/right rotation if a full turn has been completed
	if (yRotation < 0)		yRotation += 360.0f;	// compute the same rotation as for the current angle-360  
	// for example a 2 complete turns (720 degrees) will give the same orientation as if it were 0 degrees or 360

	if (m_lift > 5.0f) { m_firstPersonCamera.setTargetPosition(moveForward(m_vposition, yRotation, 1.0f, 3*xRotation)); m_bcollisionEnabled = false; }
	else 	{ m_firstPersonCamera.setTargetPosition(moveForward(m_vposition, yRotation, 1.0f, xRotation)); m_bcollisionEnabled = true; }
	
	// place the camera slightly behind the model
	m_firstPersonCamera.setCameraPosition(glm::vec3 (
		(GLfloat)	(m_vposition.x + 1.55f*sin((360.0f-yRotation)*RADIANS)), 
					 m_vposition.y + m_lift, 
		(GLfloat)	(m_vposition.z + 1.55f*cos((360.0f-yRotation)*RADIANS)))
		);
	m_firstPersonCamera.update();

	// animation control and sound control
	// this is a nested if to identify what action is taking place at this moment in the update
	// so 
	//	if is jumping, jump animation and sound
	//	if is no jumping, check 
	//		if is moving forward, move forward animation and sound
	//		if is not moving forward, check
	//			if is using, use animation and sound
	//			if is not using and not moving forward, check
	//					if is moving backwards, move backward animation and sound
	//					if is not moving backwords, not using, not moving forward, not jumping
	//							it means it stands still, so animate stand
	if (m_bisJumping) 
	{ 
		m_icurrentAnimation = MD2_JUMP; 
		if (sound_jump && m_benabled ) 
		{ 
			m_itimer	= SDL_GetTicks(); 
			m_benabled	= false; 
			m_idelay	= 750; 
			PLAYSAMPLE(sound_jump);  
		} 
	}
	else 
	{
		PAUSESAMPLE(sound_jump);
		if (m_bgoingForward) 
		{ 
			m_icurrentAnimation = MD2_RUN; 
			if (sound_walk && m_benabled) 
			{ 
				m_itimer	= SDL_GetTicks(); 
				m_benabled	= false; 
				m_idelay	= 380; 
				PLAYSAMPLE(sound_walk); 
			} 
		}
		else 
		{
			PAUSESAMPLE (sound_walk);
			m_idelay = 300;
			if (m_bactivates) 
			{ 
				m_icurrentAnimation = MD2_WAVE; 
				if(sound_use && m_benabled) { m_itimer = SDL_GetTicks(); m_benabled =false; PLAYSAMPLE(sound_use); } 
			}
			else
			{
				PAUSESAMPLE(sound_use);
				if (m_bgoingBackward) m_icurrentAnimation = MD2_FLIP;
				else m_icurrentAnimation = MD2_STAND;
			}
		}
	}

	// this timer prevents the walk sound to start playing after each update
	// after 380 ms (which is about the time needed for the model to animate
	// making a step in the running animation)
	if (SDL_GetTicks()-m_itimer > m_idelay) m_benabled = true;

	m_playerModel.Animate(m_icurrentAnimation, 0.1f);
	rt3d::updateMesh(m_mesh, RT3D_VERTEX, m_playerModel.getAnimVerts(), m_playerModel.getVertDataSize());
}

void Player::handleEvents()
{
	SDL_ShowCursor(false);
	SDL_GetMouseState(&m_iMouseX, &m_iMouseY);
	yRotation -= (XCENTRE-m_iMouseX)*0.15f;		// rotation speeds, 0.15 is just a suitable value
	xRotation += (YCENTRE-m_iMouseY)*0.005f;		// 0.005 is suitable for rotating up and down

	if (PRESSING(SDL_SCANCODE_R)) { m_lift+= 0.5f; m_firstPersonCamera.setTargetY(m_lift);};
	if (PRESSING(SDL_SCANCODE_F)) { m_lift-= 0.5f; m_firstPersonCamera.setTargetY(m_lift);};
	
	if (JOY->joysticksInitialised())
		// check controls using both joystick and keyboard
	{
		yRotation += 90*m_moveSpeed*TheInputHandler::m_joystickSensibility*JOY->xValue(0,1);
		xRotation += 0.9f* TheInputHandler::m_joystickSensibility*JOY->yValue(0,1);

		if (PRESSING(SDL_SCANCODE_LSHIFT)|| JOY->getButtonState(0,BUTTON_B)) m_moveSpeed = 0.6f;	else  m_moveSpeed = 0.3f;
		if (PRESSING(SDL_SCANCODE_W)	 || JOY->xValue(0,2) <0 )			 moveForward();			else m_bgoingForward = false; 
		if (PRESSING(SDL_SCANCODE_S)	 || JOY->xValue(0,2) >0 )			 moveBackward();		else m_bgoingBackward = false;
		if (PRESSING(SDL_SCANCODE_A)	 || JOY->getButtonState(0,LB))		 moveLeft();
		if (PRESSING(SDL_SCANCODE_D)	 || JOY->getButtonState(0,RB))		 moveRight();
		if (PRESSING(SDL_SCANCODE_E)	 || JOY->getButtonState(0,BUTTON_X)) m_bactivates = true;	else m_bactivates = false; 
		if (PRESSING(SDL_SCANCODE_SPACE) || JOY->getButtonState(0,BUTTON_A)) m_bisJumping = true;	else m_bisJumping = false;
		if (PRESSING(SDL_SCANCODE_F1)	 || JOY->getButtonState(0,LS))	 	 m_bfirstPersonCameraIsActive = true;
		if (PRESSING(SDL_SCANCODE_F2)	 || JOY->getButtonState(0,RS))		 m_bfirstPersonCameraIsActive = false;
		if (JOY->getButtonState(0,BUTTON_START))							 TheGame::Instance()->quit();
	}
	else
		// check controls without joystick
	{
		if (PRESSING(SDL_SCANCODE_LSHIFT))	m_moveSpeed = 0.6f;		else m_moveSpeed		= 0.3f;
		if (PRESSING(SDL_SCANCODE_W))		moveForward();			else m_bgoingForward	= false; 
		if (PRESSING(SDL_SCANCODE_S))		moveBackward();			else m_bgoingBackward	= false;
		if (PRESSING(SDL_SCANCODE_A))		moveLeft();
		if (PRESSING(SDL_SCANCODE_D))		moveRight();
		if (PRESSING(SDL_SCANCODE_E))		m_bactivates = true;	else m_bactivates = false; 
		if (PRESSING(SDL_SCANCODE_SPACE))	m_bisJumping = true;	else m_bisJumping = false;
		if (PRESSING(SDL_SCANCODE_F1))		m_bfirstPersonCameraIsActive = true;
		if (PRESSING(SDL_SCANCODE_F2))		m_bfirstPersonCameraIsActive = false;
	}
}

void Player::render()
{
	m_firstPersonCamera.render();
	
	SHADER->in(SHADER->top());	
		if (!m_bfirstPersonCameraIsActive) {
			SHADER->in(SHADER->top());		
			
				// rotate the character with the camera too 
				// so that if the camera rotates to left for example
				// the model rotates left by the same amount of degrees
				SHADER->m_vStack.top() = glm::translate(SHADER->top(), glm::vec3(m_vposition));
				SHADER->m_vStack.top() = glm::rotate(SHADER->top(), 90.0f-yRotation, glm::vec3(0.0f, 1.0f, 0.0f));
				SHADER->m_vStack.top() = glm::translate(SHADER->top(), glm::vec3(-m_vposition));
			
				// translate it to position
				SHADER->m_vStack.top() = glm::translate(SHADER->top(), glm::vec3(m_vposition));
			
				// this rotation is just because the model is not properly oriented at export
				SHADER->m_vStack.top() = glm::rotate(SHADER->top(), 90.0f, glm::vec3(-1.0f, 0.0f, 0.0f));

				// this scale is to adjust the size of the model
				SHADER->m_vStack.top() = glm::scale(SHADER->top(),glm::vec3(0.03, 0.03, 0.03));

				SHADER->updateValue("modelview");

				// show only visible side 
				glCullFace(GL_FRONT);
				// attach texture to model
				glBindTexture(GL_TEXTURE_2D, m_texture);
				// send model material parameters to shader
				SHADER->updateMaterial(m_material);
				// draw 
				rt3d::drawMesh(m_mesh, m_ivertexCount, GL_TRIANGLES);		

			SHADER->out();
		}
	glCullFace(GL_BACK);
	SHADER->out();
	
	SHADER->out();	// this pop is for the camera
}
