#include "MenuState.h"
#include "Game.h"
#include "PlayState.h"
#include "AboutState.h"
#include "ControlsState.h"

const std::string MenuState::s_menuID = "MENU";
const int NUMBEROFOPTIONS = 4;

void MenuState::render()
{
	glClearColor(0.0f, 0.0f, 0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, m_option[m_choice]);
	rt3d::drawMesh(m_mesh,6,GL_TRIANGLES);
}

void MenuState::handleEvents()
{
	if (PRESSING(SDL_SCANCODE_UP)) 
	{
		if (m_choice == 0) m_choice = NUMBEROFOPTIONS-1;
		else m_choice --;
		// delay needed to avoid changing options too quickly
		SDL_Delay(150);
	}

	if (PRESSING(SDL_SCANCODE_DOWN)) 
	{
		if (m_choice == NUMBEROFOPTIONS-1) m_choice = 0;
		else m_choice ++;
		// delay needed to avoid changing options too quickly
		SDL_Delay(200);
	}

	if ( PRESSING(SDL_SCANCODE_RETURN) )
	{
		switch (m_choice)
		{
		case NEWGAME:	SHADER->unlink();
			TheGame::Instance()->getStateMachine()->pushState(new PlayState());
			break;
		case CONTROLS:	TheGame::Instance()->getStateMachine()->pushState(new ControlsState());
			break;
		case ABOUT:		TheGame::Instance()->getStateMachine()->pushState(new AboutState());
			break;
		case QUIT:		TheGame::Instance()->quit();
			break;
		}
	}

	if (PRESSING(SDL_SCANCODE_ESCAPE))
	{
		TheGame::Instance()->quit();
		SDL_Delay(150);
	}
}

bool MenuState::onEnter()
{
	if (SHADER->isMenuOn())
		SHADER->useMenuShader();
	else
		SHADER->init(MENUSHADER);

	m_choice = NEWGAME;					// new game will be highlighted by default (0)
	
	// set the rectangle that covers all the screen
	rt3d::lightStruct light = 
	{
		{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
		{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		{0.5f, 0.5f, 0.5f, 0.0f}  // position
	};
	SHADER->updateLight(MENUSHADER,light);

	rt3d::materialStruct material = 
	{
		{1.0f, 1.0f, 1.0f, 1.0f},	// ambient
		{1.0f, 1.0f, 1.0f, 1.0f},	// diffuse
		{1.0f, 1.0f, 1.0f, 1.0f},	// specular
		2.0f						// shininess
	};
	SHADER->updateMaterial(MENUSHADER,material);

	float backgroundSize = 0.58f;	
	float aspectRatio = (float) WIDTH/HEIGHT;
	float backgroundWidth = backgroundSize * aspectRatio;

	GLfloat cubeVerts[] = 
	{
		// first triangle
		-backgroundWidth, -backgroundSize, -1, 
		 backgroundWidth,  backgroundSize, -1,
	 	 backgroundWidth, -backgroundSize, -1,
		// second triangle	
		-backgroundWidth, -backgroundSize, -1,
		-backgroundWidth,  backgroundSize, -1,
		 backgroundWidth,  backgroundSize, -1,
	};

	GLfloat cubeTexCoords[] = 
	{ 
		0, 1,  1, 0,  1, 1,   
		0, 1,  0, 0,  1, 0 
	};

	// load the background pictures to for each option
	m_option[NEWGAME] = TheTextureManager::Instance()->loadImage("data/texture/newgame.bmp");
	m_option[CONTROLS] = TheTextureManager::Instance()->loadImage("data/texture/controls.bmp");
	m_option[ABOUT] = TheTextureManager::Instance()->loadImage("data/texture/about.bmp");
	m_option[QUIT] = TheTextureManager::Instance()->loadImage("data/texture/quit.bmp");
	
	// create the mesh
	m_mesh = rt3d::createMesh(6, cubeVerts, nullptr, cubeVerts, cubeTexCoords);//, 6, cubeIndices);

	SHADER->updateValue(MENUSHADER,"projection", glm::perspective(60.0f,(float) WIDTH/HEIGHT,1.0f,1500.0f));
	SHADER->updateValue(MENUSHADER,"modelview", glm::mat4 (1.0f));		
	return true;
}

bool MenuState::onExit()
{
	SHADER->unlink();
	return true;
}