// This skybox was created with the help of William Taylor 
// because with the RT3D library, I couldn`t bind the textures correctly
// I strongly believe that this is the right method I should have
// learned on how to use openGL in 3D programming
// fortunately I understood quite well how it goes using RT3D so it 
// will not be too difficult to actually go on lower levels of 3d programming
// using VAOs and VBOs 

// the class was refactored to fit my program and to fit the properties of 
// an abstract object

#ifndef __Skybox__
#define __Skybox__

#include "AbstractObject.h"

class Skybox:
	public AbstractObject
{
private:

	GLuint m_textures[5];
	GLuint m_VBO[5];
	GLuint m_VAO;
	
	void initialiseVBO(GLfloat*, int vboNum);
	void load(const std::string& filename);
	void createSkyboxCube();

public:

	Skybox(void) {}
	~Skybox(void) {}

	void init();
	void update() {}
	void handleEvents() {}
	void render();

};

#endif // defined (__Skybox__)