#ifndef __MenuState__
#define __MenuState__

#include "AbstractGameState.h"

class MenuState :
	public AbstractGameState
{
private:

	unsigned int m_choice;
	GLuint m_mesh;
	GLuint m_option[4];
	
	static const std::string s_menuID;

public:

	MenuState(void) {}
	~MenuState(void) {}

	void update() {}
	void render();
	void handleEvents();

	bool onEnter();
	bool onExit();

	std::string getStateID() const { return s_menuID; }
};

#endif // defined (__MenuState__)