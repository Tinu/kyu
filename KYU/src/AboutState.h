#ifndef __AboutState__
#define __AboutState__

#include "AbstractGameState.h"

class AboutState :
	public AbstractGameState
{
private:

	GLuint m_mesh;
	GLuint m_background;

	static const std::string s_menuID;

public:

	AboutState(void) {}
	~AboutState(void) {}

	void update() {}
	void render();
	void handleEvents();

	bool onEnter();
	bool onExit();

	std::string getStateID() const { return s_menuID; }
};

#endif // defined (__AboutState__)