#ifndef __SoundManager__
#define __SoundManager__

#include <iostream>
#include "bass.h"

#define PLAYSAMPLE SoundManager::Instance()->playSound
#define PAUSESAMPLE SoundManager::Instance()->pauseSound
#define LOADSAMPLE SoundManager::Instance()->load

class SoundManager
{
private:
	
	SoundManager(void) 
	{
		if (!BASS_Init(-1,44100,0,0,0))
		std::cout << "Can't initialize device\n";
	}

	static SoundManager* s_pInstance;
	HSAMPLE *samples[2];

public:

	static SoundManager* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new SoundManager();
			return s_pInstance;
		}
		return s_pInstance;
	}

	HSAMPLE load(char * filename);
	void playSound(HSAMPLE &sound);
	void pauseSound(HSAMPLE &sound);
	void resume();
	
	~SoundManager(void) {}
};

typedef SoundManager TheSoundManager;

#endif // defined (__SoundManager__)