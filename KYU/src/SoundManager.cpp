#include "SoundManager.h"

SoundManager * SoundManager::s_pInstance = 0;

HSAMPLE SoundManager::load(char * filename)
{
	HSAMPLE sample;
	sample = BASS_SampleLoad(FALSE,filename,0,0,3,BASS_SAMPLE_OVER_POS);

	if (sample) return sample;
	else 
	{
		std::cout << "could not load sound sample " << filename <<"\n";
		return -1;
	}
}

void SoundManager::playSound(HSAMPLE &sound)
{
	HCHANNEL ch=BASS_SampleGetChannel(sound,FALSE);
	BASS_ChannelSetAttributes(ch,-1,50,(rand()%201)-100);
	
	if (!BASS_ChannelPlay(ch,FALSE))
		std::cout << "Can't play sample\n";
}

void SoundManager::pauseSound(HSAMPLE &sound)
{
	BASS_SampleStop(sound);
}

void SoundManager::resume()
{
	BASS_Start();
}