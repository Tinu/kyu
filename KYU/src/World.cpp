#include "World.h"
#include "TextureManager.h"
//#include <ios>

void World::init()
{
	char buffer[128];
	// loading all textures and objects
	// textures and models should be named consecutively
	// and follow a certain format textureXX (where XX is the 
	// last texture value + 1) and same for modelXX
	for (int i = 0; i !=NUMBEROFOBJECTS; i++) 
	{
		std::string temp = _itoa(i,buffer,10);
		std::string path = "data/texture/texture"+temp+".bmp";
		m_vTexture.push_back (LOADIMAGE((char*) path.c_str()));
		// textures loaded in m_vTexture
		path = "data/model/model"+temp+".obj";
		rt3d::loadObj((char*) path.c_str(), m_vVerts, m_vNormals, m_vTextureLocation, m_vIndices);
		m_iIndexCount[i] = m_vIndices.size();
		m_vMesh.push_back(
			rt3d::createMesh(
			m_vVerts.size()/3, 
			m_vVerts.data(), 
			nullptr, 
			m_vNormals.data(),
			m_vTextureLocation.data(), 
			m_iIndexCount[i], 
			m_vIndices.data()));
		// models loaded in m_vMesh
	}
	rt3d::materialStruct tempMaterial = 
	{
		{2.4f, 2.4f, 2.3f, 1.0f}, // ambient
		{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
		{0.0f, 0.1f, 0.0f, 1.0f}, // specular
		180.0f  // shininess
	};

	m_material = tempMaterial;
}

void World::render()  
{
	SHADER->in(SHADER->top());

		SHADER->updateValue("modelview");
		// send World Model Material to the shader
		SHADER->updateMaterial(m_material);
		for (int i=0; i!=NUMBEROFOBJECTS; i++) 
		{
			// atach texture to all the loaded models
			glBindTexture(GL_TEXTURE_2D, m_vTexture[i]);
			// draw
			rt3d::drawMesh(m_vMesh[i], m_iIndexCount[i], GL_TRIANGLES);
		}
		
	SHADER->out();
}