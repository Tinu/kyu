#ifndef __TextureManager__
#define __TextureManager__

#include <glew.h>

#ifndef LOADIMAGE
#define LOADIMAGE TextureManager::Instance()->loadImage
#endif

class TextureManager
{
public:

	static TextureManager* Instance()
	{
		if(s_pInstance == 0)
		{
			s_pInstance = new TextureManager();
			return s_pInstance;
		}

		return s_pInstance;
	}

GLuint loadImage(char *fname);

private:
	static TextureManager* s_pInstance;
	TextureManager(void) {}
};

typedef TextureManager TheTextureManager;

#endif // defined (__TextureManager__)