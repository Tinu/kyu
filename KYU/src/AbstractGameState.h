#ifndef __AbstractGameState__
#define __AbstractGameState__

#include "Header.h"

class AbstractGameState
{
public:
	virtual void update() = 0;
	virtual void render() = 0;
	virtual void handleEvents() = 0;

	virtual bool onEnter() = 0;
	virtual bool onExit() = 0;

	virtual std::string getStateID() const = 0;
};

#endif // defined (__AbstractGameState__)