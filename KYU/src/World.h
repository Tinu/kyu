#ifndef __World__
#define __World__

#include "AbstractObject.h"

#define NUMBEROFOBJECTS 17

class World:public AbstractObject
{
private:

	std::vector<GLfloat> m_vVerts;
	std::vector<GLfloat> m_vNormals;
	std::vector<GLfloat> m_vTextureLocation;
	std::vector<GLuint> m_vIndices;
	std::vector<GLuint> m_vTexture;
	
	std::vector<GLuint> m_vMesh;

	GLuint m_iIndexCount[NUMBEROFOBJECTS];
	rt3d::materialStruct m_material;
	
public:

	World(void) {}
	~World(void) {}

	void init();
	void render();
	void update() {};
	void handleEvents() {};
	
};

#endif // defined (__World__)