#include "IntroState.h"
#include "Game.h"
#include "MenuState.h"

const std::string IntroState::s_menuID = "INTRO";

const int DELAY_IN_MILLISECONDS = 5000;

void IntroState::update()
{
	m_iframeTime = SDL_GetTicks() - m_iframeStart;
	// count the time past
	if (m_iframeTime > DELAY_IN_MILLISECONDS)
		// when it`s bigger than the delay, change the state
	{
		TheGame::Instance()->getStateMachine()->pushState(new MenuState());
	}
	// add an amout to alfa with every update
	if (m_alfa<1.0f)	m_alfa += (float) (m_iframeTime / 300000.0f);
	// once alfa it`s 1, it`s useless to continue adding, so set to 1
	else (m_alfa = 1.0f);

	rt3d::materialStruct material = 
	{
		// for a quick effect from dark to full light
		// go through all components from 0 to 1
		{m_alfa, m_alfa, m_alfa, m_alfa},	// ambient
		{m_alfa, m_alfa, m_alfa, m_alfa},	// diffuse
		{m_alfa, m_alfa, m_alfa, m_alfa},	// specular
		2.0f						// shininess
	};
	SHADER->updateMaterial(MENUSHADER,material);
}

void IntroState::render()
{
	glClearColor(0.0f, 0.0f, 0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, m_background);
	rt3d::drawMesh(m_mesh,6,GL_TRIANGLES);
}

void IntroState::handleEvents()
{
	// will go to next state on Space or on Escape
	if ( PRESSING(SDL_SCANCODE_SPACE)|| PRESSING(SDL_SCANCODE_ESCAPE) )
	{
		SDL_Delay(200);
		TheGame::Instance()->getStateMachine()->pushState(new MenuState());
	}
}

bool IntroState::onEnter()
{	
	// -0.1 because 0 starts too quickly to show the background picture
	m_alfa = -0.1f;
	if (SHADER->isMenuOn())
		SHADER->useMenuShader();
	else
		SHADER->init(MENUSHADER);

	// set the rectangle that covers all the screen
	rt3d::lightStruct light = 
	{
		{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
		{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		{0.5f, 0.5f, 0.5f, 0.0f}  // position
	};

	SHADER->updateLight(MENUSHADER,light);
	float backgroundSize = 0.66f;
	float aspectRatio = (float) WIDTH/HEIGHT;
	float backgroundWidth = backgroundSize * aspectRatio;
	// need to keep the same aspectRatio for the logo

	GLfloat cubeVerts[] = 
	{
		// first triangle
		-backgroundWidth, -backgroundSize, -1, 
		 backgroundWidth,  backgroundSize, -1,
		 backgroundWidth, -backgroundSize, -1,
		// second triangle	
		-backgroundWidth, -backgroundSize, -1,
		-backgroundWidth,  backgroundSize, -1,
		 backgroundWidth,  backgroundSize, -1,
	};

	GLfloat cubeTexCoords[] = 
	{ 
		0, 1,  1, 0,  1, 1,   
		0, 1,  0, 0,  1, 0 
	};

	// load the background picture to bind to the rectangle
	m_background = TheTextureManager::Instance()->loadImage("data/texture/background.bmp");

	// create the mesh
	m_mesh = rt3d::createMesh(6, cubeVerts, nullptr, cubeVerts, cubeTexCoords);

	SHADER->updateValue(MENUSHADER,"projection", glm::perspective(60.0f,(float) WIDTH/HEIGHT,1.0f,1500.0f));
	SHADER->updateValue(MENUSHADER,"modelview", glm::mat4 (1.0f));		

	m_iframeStart = SDL_GetTicks();
	return true;
}

bool IntroState::onExit()
{
	SHADER->unlink();
	return true;
}