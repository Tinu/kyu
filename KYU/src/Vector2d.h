// This is a reusable vector2d class created by Constantin Toader
// after taking the module Game Engine Design
// It includes overoaded operators and basic functions for 2D Vectors

#ifndef __Vector2d__
#define __Vector2d__
#include <math.h>

class Vector2D
{
private:
	float m_x;
	float m_y;
public:
	Vector2D(): m_x(0.0f), m_y(0.0f) {}
	// default constructor with values set to 0;

	Vector2D(const Vector2D& toCopy): m_x(toCopy.m_x), m_y(toCopy.m_y) {}
	// copy constructor

	Vector2D(float x, float y): m_x(x), m_y(y) {}
	// constructor that assigns values by components

	float getX() { return m_x; }
	float getY() { return m_y; }
	int getIntX() { return (int) m_x; }
	int getIntY() { return (int) m_y; }

	void setX(float x) { m_x = x; }
	void setY(float y) { m_y = y; }
	void resetToZero() { m_x = m_y = 0; }
	void resetToOne() { m_x = m_y = 1; }
	void resetToValue(float value) { m_x = m_y = value; }

	float length() { return sqrt(m_x * m_x + m_y * m_y); }

	// ------------------------------- operators ------------------------------------

	Vector2D operator+(const Vector2D& rightHandSide) const
		// addition of 2 vectors:
	{
		return Vector2D(m_x+ rightHandSide.m_x, 
						m_y+ rightHandSide.m_y);
	}


	friend Vector2D& operator +=(Vector2D& leftHandSide, const Vector2D& rightHandSide)
		// adds the right hand side vector components to left hand side vector
	{
		leftHandSide.m_x += rightHandSide.m_x;
		leftHandSide.m_y += rightHandSide.m_y;
		return leftHandSide;
	}


	Vector2D operator*(float scalar) 
		// multiply vector components by a scalar and return the result
	{
		return Vector2D(this->m_x * scalar, this->m_y * scalar);
	}

	
	Vector2D& operator *= (float scalar)
		// multiply vector components by a scalar and store the result
		// in the left hand side vector
	{
		m_x *= scalar;
		m_y *= scalar;
		return *this;
	}


	Vector2D operator-(const Vector2D& rightHandSide) const
		// subtraction of 2 vectors:
	{
		return Vector2D(m_x- rightHandSide.m_x, 
						m_y- rightHandSide.m_y);
	}


	friend Vector2D& operator -=(Vector2D& leftHandSide, const Vector2D& rightHandSide)
		// subtracts the right hand side vector components from the
		// left hand side vector
	{
		leftHandSide.m_x -= rightHandSide.m_x;
		leftHandSide.m_y -= rightHandSide.m_y;
		return leftHandSide;
	}


	Vector2D operator/(float scalar) 
		// divide vector components by a scalar and return the result
	{
		return Vector2D(this->m_x / scalar, this->m_y / scalar);
	}

	
	Vector2D& operator /= (float scalar)
		// divide vector components by a scalar and store the result
		// in the left hand side vector
	{
		m_x /= scalar;
		m_y /= scalar;
		return *this;
	}


	bool operator==(Vector2D& rightHandSide)
		// method to check if two vectors are identical
		// mathematical version
	{
		return isEqualTo(rightHandSide);
	}


	bool operator!=(Vector2D& rightHandSide) 
		// method to check if two vectors are NOT equal
	{
		return (!isEqualTo(rightHandSide));
	}

	
	// ----------------------------- Functionality -----------------------------------
	
	void normalize()
		// method to convert to unit vector
	{
		float tempLength (length());
		if ( tempLength> 0) 
			// avoid division by 0
		{
			*this *= 1/ tempLength;
		}
	}

	float dot(Vector2D& rightHandSide)
		// method that returns the dot product
		// only if none of the vectors are not 0
	{
		if (m_x != 0 && m_y != 0 && rightHandSide.m_x != 0 && rightHandSide.m_y != 0)
			return this->m_x*rightHandSide.m_x+ this->m_y*rightHandSide.m_y;
	}

	bool isPerpendicularOn(Vector2D& rightHandSide)
		// method that returns true if perpendicular
	{
		if (dot(rightHandSide)==0) 
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	bool isEqualTo(Vector2D& rightHandSide)
		// method to check if two vectors are identical
		// literal version
	{
		if (this->m_x == rightHandSide.m_x && this->m_y == rightHandSide.m_y)
			return true;
		return false;
	}


};

#endif // defined (__Vector2d__)
