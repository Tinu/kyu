#ifndef __Camera__
#define __Camera__

#include "AbstractObject.h"

class Camera: public AbstractObject
{
private:

	GLfloat m_viewAngle;								// viewing angle for the viewing volume
	GLfloat m_aspectRatio;								// aspect ratio for the viewing volume
	GLfloat m_near;										// starting distance from the eye of the viewing volume
	GLfloat m_far;										// distance of viewing volume
	glm::mat4 m_projection, m_view;
	glm::vec3 m_veye, m_vat, m_vup;

public:

	Camera(void) {}
	~Camera(void) {}

	glm::vec3 *eyePoint()	 { return &m_veye; }
	glm::vec3 *atPoint()	 { return &m_vat; }
	glm::vec3 *upDirection() { return &m_vup; }

	void setCameraPosition	(glm::vec3 pos) { m_veye = pos; };
	void setTargetPosition	(glm::vec3 pos) { m_vat = pos; };
	void setOrientation		(glm::vec3 pos) { m_vup = pos; };

	void setTargetY(GLfloat y) { m_vat.y = y; }

	void init();
	void render();
	void handleEvents() {}
	void update();
	
};

#endif // defined (__Camera__)