#include "Camera.h"

void Camera::init()
{
	m_viewAngle = 70.0f;
	m_aspectRatio = (float) WIDTH/(float) HEIGHT;
	m_near = 0.5f;
	m_far = 1500.0f;	// 1500 metres should be enough for the view
	// normally should be more than 1000*sqrt(2) which is the length
	// of the diagonal of the skybox base, from one corner to another (1414m).

	// using shader class
	// push identity
	SHADER->in(glm::mat4(1.0f));							
	// add projection 
	SHADER->m_vStack.top() = glm::perspective(m_viewAngle,m_aspectRatio,m_near, m_far);
	// send to shader
	SHADER->updateValue("projection");

	// old style using rt3d
	//	projection = glm::perspective(viewAngle,aspectRatio,near, far);
	//	rt3d::setUniformMatrix4fv(SHADER->getIt(), "projection", glm::value_ptr(projection));

	setCameraPosition(glm::vec3(0.0f, 1.0f, 0.0f));
	setTargetPosition(glm::vec3(0.0f, 1.0f, -1.0f));
	setOrientation(glm::vec3 (0.0f, 1.0f, 0.0f));
	
}

void Camera::update()
{
	m_view = glm::lookAt(m_veye,m_vat,m_vup);	
}

void Camera::render() 
{
	SHADER->in(m_view);
	SHADER->in(SHADER->top());
}
	
	