// This ModelLoader is used to load collisions from obj files
// The file reads obj files which contains only lines that connect
// the two points of a rectangle that form a diagonal
// it stores them in a vector in the order that is being read by the
// collision checker.

#ifndef __ModelLoader__
#define __ModelLoader__

#include <vector>
#include <fstream>
#include <string>
#include <iostream>


class ModelLoader
{
private:
	static ModelLoader* s_pInstance;
	ModelLoader(void) {}


	struct point
		// will hold points position on 2 axis
	{
		float x, y;
		
		point(float xpos, float ypos, float zpos) : x(xpos), y(ypos) {}
		point(float xpos, float ypos): x(xpos), y(ypos) {}
	};

	int m_inumberOfTotalVertices;
	int m_inumberOfPoints;
	std::vector<point*> m_points;
	std::vector<float> m_sortedCoordinates;

public:
	void showData();
	static ModelLoader* Instance()
	{
		if (s_pInstance == 0) 
		{
			s_pInstance = new ModelLoader();
			return s_pInstance;
		}
		return s_pInstance;
	}
	~ModelLoader(void);
	std::vector<float> loadObj(const char* fileName);
	std::vector<point*> getPoints()		{ return m_points; }
	std::vector<float> getCoordinates() { return m_sortedCoordinates; }
	int getNumberOfPoints()				{ return m_inumberOfPoints; }
	void clear();
};

typedef ModelLoader TheModelLoader;

#endif // defined (__ModelLoader__)