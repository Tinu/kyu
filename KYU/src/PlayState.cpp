#include "PlayState.h"
#include "Light.h"
#include "Player.h"
#include "World.h"
#include "Skybox.h"
#include "Game.h"
#include "MenuState.h"

const std::string PlayState::s_menuID = "PLAY";

void PlayState::update()
{
	for (unsigned int i=0; i!= m_vobject3D.size(); i++)
	{
		m_vobject3D[i]->update();
	}
}

void PlayState::render()
{
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_CULL_FACE);

	glm::mat4 modelview(1.0f);

	SHADER->in(modelview);
	for (unsigned int i=0; i!= m_vobject3D.size(); i++)
	{
		m_vobject3D[i]->render();
	}
	SHADER->out();

	glCullFace(GL_BACK);
	glDepthMask(GL_TRUE);
}

void PlayState::handleEvents()
{
	// only handle the first object which is the player
	m_vobject3D[0]->handleEvents();

	if (PRESSING(SDL_SCANCODE_ESCAPE))
	{
		TheGame::Instance()->quit();
	}

	if ( PRESSING(SDL_SCANCODE_1) )
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_CULL_FACE);
	}
	if ( PRESSING(SDL_SCANCODE_2) ) 
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_CULL_FACE);
	}
}

bool PlayState::onEnter()
{
	TheInputHandler::Instance()->initialiseJoysticks();
	if (SHADER->isGameOn())
		SHADER->useGameShader();
	else
		SHADER->init(GAMESHADER);

	m_vobject3D.push_back(new Player());
	m_vobject3D.push_back(new Light());
	m_vobject3D.push_back(new World());
	m_vobject3D.push_back(new Skybox());

	for (unsigned int i=0; i!= m_vobject3D.size(); i++)
	{
		m_vobject3D[i]->init();
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	return true;
}

bool PlayState::onExit()
{
	return true;
}

void PlayState::clean()
{
	for (unsigned int i=0; i!= m_vobject3D.size(); i++)
	{
		delete m_vobject3D[i];
	}
}