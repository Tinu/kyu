#ifndef __Game__
#define __Game__

#include "GameStateMachine.h"

class Game
{
private:
	static Game* s_pInstance;
	Game() {}			// private constructor for singleton
	SDL_Window * setupRenderingContext(SDL_GLContext &context, const char* title, 
		int xpos, int ypos, int width, int height, bool fullscreen);
	bool m_bRunning;

	SDL_Window* m_pWindow;
	SDL_GLContext m_glContext;
	GameStateMachine* m_pGameStateMachine;	

public:

	static Game* Instance()				// game singleton
	{
		if(s_pInstance == 0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}

		return s_pInstance;
	}
	

	
	bool init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);

	void render();
	void update();
	void handleEvents();

	void quit();
	bool running()						{ return m_bRunning; }

	void clean() {}
	
	SDL_Window * getWindow()			{ return m_pWindow; }
	GameStateMachine* getStateMachine() { return m_pGameStateMachine; }
};

// a more decent name
typedef Game TheGame;

#endif // defined (__Game__)
