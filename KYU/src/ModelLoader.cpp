#include "ModelLoader.h"
#include <sstream>

ModelLoader* ModelLoader::s_pInstance = 0;

void sort(float first, float second, float &minimum, float &maximum) 
	{ 
		if (first > second) 
		{
			minimum = second - 1.0f;		// 1.35 is the offset to apply to the collision mask
			maximum = first + 1.0f;		// otherwise the model will intersect the buildings 
		} 
		else
		{
			minimum = first - 1.0f;
			maximum = second + 1.0f;
		}
	}

ModelLoader::~ModelLoader(void)
{
	// cleanup to avoid memory leaks	
	for (unsigned int i=0; i<m_points.size(); i++)			{ delete m_points[i]; }
}

std::vector<float> ModelLoader::loadObj(const char* filename)
{
	m_inumberOfTotalVertices =0;
	m_inumberOfPoints =0;
	std::ifstream in(filename);		// file to load
	char buffer[128];				// buffer to read lines

	if (!in.is_open())
	{
		std::cout << "File has not been loaded ... \n";
		//return false;
	}
	while (!in.eof())
		// read all data in a string
	{
		in.getline(buffer,128);

		// ========================= vertex loading =================================
	if (buffer[0] =='v' && buffer[1]==' ')
			// if it starts with a v followed by space
			// it is a vertex, so store it
		{
			float localX, localY, localZ;
			sscanf_s(buffer, "v %f %f %f",&localX, &localY, &localZ);
			m_points.push_back(new point(localX, localZ));
			m_inumberOfTotalVertices++;
		}		
	}
	
	
	float min(0.0f), max(0.0f);
	

	for (unsigned int i=0; i!= m_inumberOfTotalVertices; i+=2)
	// so the collision files store lines which represent the diagonal
	// of a rectangle that is the base of each building, so basically for 
	// each building we will have two points: xy coordinates for each one
	// the difference between x of both points is the width while the difference
	// between both y is the height
	// this way using two points we actually have the coordinates of all 4 edges
	// of the rectangle.
	// this way we find out which x is on the left edge and which x is on the right edge
	// which y is on the top edge and which y is on the bottom edge
	// this way we will store our points in the right order to be easier checked for 
	// collision later on when we check if the object suspect for collision is higher than
	// the bottom and lower than the top, on the right side of the left edge and on the left 
	// side of the right edge. If so, then there is a collision

	// so using the minimum and maximum functions we find the min x (left edge), max x (right edge)
	// store them and the proceed with the same thing for the y values
	{
		sort(m_points[i]->x, m_points[i+1]->x, min, max);
		m_sortedCoordinates.push_back(max);
		m_sortedCoordinates.push_back(min);
		sort(m_points[i]->y, m_points[i+1]->y, min, max);
		m_sortedCoordinates.push_back(max);
		m_sortedCoordinates.push_back(min);
	}
	return m_sortedCoordinates;
}

void ModelLoader::clear()
	// this function empties the vectors if the model loader is needed 
	// to load a different collision mask
{
	m_sortedCoordinates.clear();
	m_points.clear();
}

void ModelLoader::showData() 
	// this function is only for testing
{
		for (unsigned int i=0; i!= m_inumberOfPoints-1; i+=4)
	{
		std::cout << "Rectangle " << i;
		std::cout << " X1: " << m_sortedCoordinates[i] << " Y1: " << m_sortedCoordinates[i+1];
		std::cout << " X2: " << m_sortedCoordinates[i+2] << " Y2: " << m_sortedCoordinates[i+3] << "\n";			
	}
}