#ifndef __IntroState__
#define __IntroState__

#include "AbstractGameState.h"

class IntroState:
	public AbstractGameState
{
private:

	Uint32 m_iframeStart, m_iframeTime;
	float m_alfa;			// holds the value to modify the colour component
	
	GLuint m_background;
	GLuint m_mesh;

	static const std::string s_menuID;

public:

	IntroState(void) {}
	~IntroState(void) {}

	void update();
	void render();
	void handleEvents();

	bool onEnter();
	bool onExit();

	virtual std::string getStateID() const { return s_menuID; }
};

#endif // defined (__IntroState__)