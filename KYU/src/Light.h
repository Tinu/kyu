#ifndef __Light__
#define __Light__

#include "AbstractObject.h"

// the amount to change the light values by
#define MODIFIER 0.01f

class Light: public AbstractObject
{
private:

	rt3d::lightStruct m_light;
	bool m_bhasChanged;

public:

	Light(void) {}
	~Light(void) {}

	glm::vec4 position;
	Colour ambient, diffuse, specular;

	void init();
	void render();
	void handleEvents();
	void update();

	// ambient operations
	inline void increaseAmbientRed()	{ ambient.changeRed		(MODIFIER); m_bhasChanged = true; }
	inline void increaseAmbientGreen()  { ambient.changeGreen	(MODIFIER); m_bhasChanged = true; }
	inline void increaseAmbientBlue()	{ ambient.changeBlue	(MODIFIER); m_bhasChanged = true; }

	inline void decreaseAmbientRed()	{ ambient.changeRed		(-MODIFIER); m_bhasChanged = true; }
	inline void decreaseAmbientGreen()  { ambient.changeGreen	(-MODIFIER); m_bhasChanged = true; }
	inline void decreaseAmbientBlue()   { ambient.changeBlue	(-MODIFIER); m_bhasChanged = true; }

	// diffuse operations
	inline void increaseDiffuseRed()	{ diffuse.changeRed		(MODIFIER); m_bhasChanged = true; }
	inline void increaseDiffuseGreen()  { diffuse.changeGreen	(MODIFIER); m_bhasChanged = true; }
	inline void increaseDiffuseBlue()	{ diffuse.changeBlue	(MODIFIER); m_bhasChanged = true; }

	inline void decreaseDiffuseRed()	{ diffuse.changeRed		(-MODIFIER); m_bhasChanged = true; }
	inline void decreaseDiffuseGreen()  { diffuse.changeGreen	(-MODIFIER); m_bhasChanged = true; }
	inline void decreaseDiffuseBlue()   { diffuse.changeBlue	(-MODIFIER); m_bhasChanged = true; }

	// specular operations
	inline void increaseSpecularRed()	{ specular.changeRed	(MODIFIER); m_bhasChanged = true; }
	inline void increaseSpecularGreen() { specular.changeGreen	(MODIFIER); m_bhasChanged = true; }
	inline void increaseSpecularBlue()	{ specular.changeBlue	(MODIFIER); m_bhasChanged = true; }

	inline void decreaseSpecularRed()	{ specular.changeRed	(-MODIFIER); m_bhasChanged = true; }
	inline void decreaseSpecularGreen() { specular.changeGreen	(-MODIFIER); m_bhasChanged = true; }
	inline void decreaseSpecularBlue()  { specular.changeBlue	(-MODIFIER); m_bhasChanged = true; }

	void reset();
	void applyChanges();

	inline void increaseCoordinateX() { position.x += 0.1f;  m_bhasChanged = true; std::cout << "X : " << position.x << "\n";}
	inline void increaseCoordinateY() { position.y += 0.01f; m_bhasChanged = true; std::cout << "Y : " << position.y << "\n";}
	inline void increaseCoordinateZ() { position.z += 0.1f;  m_bhasChanged = true; std::cout << "Z : " << position.z << "\n";}
	inline void decreaseCoordinateX() { position.x -= 0.1f;  m_bhasChanged = true; std::cout << "X : " << position.x << "\n";}
	inline void decreaseCoordinateY() { position.y -= 0.01f; m_bhasChanged = true; std::cout << "Y : " << position.y << "\n";}
	inline void decreaseCoordinateZ() { position.z -= 0.1f;  m_bhasChanged = true; std::cout << "Z : " << position.z << "\n";}

};

#endif // defined (__Light__)