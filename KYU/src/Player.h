#ifndef __Player__
#define __Player__

#include "AbstractObject.h"
#include "md2model.h"
#include "Camera.h"
#include "SoundManager.h"

class Player: public AbstractObject
{
private:

	// model properties
	GLuint m_texture;
	GLuint m_mesh;
	int m_ivertexCount;
	md2model m_playerModel;
	int m_icurrentAnimation;
	glm::vec3 m_vposition;
	GLfloat xRotation, yRotation, zRotation;
	rt3d::materialStruct m_material;

	// animation
	bool m_bgoingForward;
	bool m_bgoingBackward;
	bool m_bisJumping;
	bool m_bactivates;

	// sound 
	HSAMPLE sound_walk;
	HSAMPLE sound_jump;
	HSAMPLE sound_use;
	bool m_benabled;				// this will prevent the sound from looping
	Uint32 m_itimer, m_idelay;			// intervals used for periods when the sound should NOT loop



	//camera
	Camera m_firstPersonCamera;
	bool m_bfirstPersonCameraIsActive;
	GLfloat m_lift;
	

	// movement
	void moveForward();
	void moveBackward();
	void moveRight();
	void moveLeft();
	int m_iMouseX, m_iMouseY;
	GLfloat m_moveSpeed;
	glm::vec3 moveForward	(glm::vec3 pos, GLfloat d);
	glm::vec3 moveRight		(glm::vec3 pos, GLfloat d);
	glm::vec3 moveForward	(glm::vec3 pos, GLfloat angle, GLfloat d, GLfloat rot);
	glm::vec3 moveRight		(glm::vec3 pos, GLfloat angle, GLfloat d, GLfloat rot);
	
	
	
	// collision
	bool m_bcollisionEnabled;
	bool checkCollison();
	std::vector<GLfloat> m_vleftMap;			// left collision map
	std::vector<GLfloat> m_vrightMap;			// right collision map
	glm::vec3 m_vnextStep[4];					// holds next step coordinates for 4 directions
	bool m_bcanMove[4];

public:

	Player(void) {}
	~Player(void) {}

	

	void init(); 
	void render();
	void update();
	void handleEvents();

};

#endif // defined (__Player__)