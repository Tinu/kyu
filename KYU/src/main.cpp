// This application was created by Constantin Toader for the Real Time 3D Module
// using rt3d utility library provided by Daniel Livingstone
// Uses OpenGL, SDL, GLM and BASS
// 22/03/2014
// Author: Constantin Toader
// The base project can be found at: https://bitbucket.org/dlivingstone/rt3d-lab-2
// to change some settings modify the Header.h file



// Windows specific: Uncomment the following line to open a console window for debug output
//#if _DEBUG
//#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
//#endif

const int FPS = 60;
const float DELAYTIME = 1000.0f / FPS;
#include "game.h"

// comment one of these to choose between fullscreen or window
#define FULLSCREEN true
//#define FULLSCREEN false

using namespace std;

int main(int argc, char *argv[]) {

	Uint32 frameStart, frameTime;
	if (TheGame::Instance()->init(
		"SDL PROJECT", 
		CENTER, 
		CENTER,
		WIDTH, HEIGHT, FULLSCREEN))
	{		
			while (TheGame::Instance()->running())
			{
				frameStart = SDL_GetTicks();
				TheGame::Instance()->update();
				TheGame::Instance()->render();
				TheGame::Instance()->handleEvents();
	
				frameTime = SDL_GetTicks() - frameStart;
				if(frameTime < DELAYTIME)						// fps sync
				{
					SDL_Delay((int) (DELAYTIME - frameTime));
				}		
			}
	}
	else
	{
		std::cout << "game init failed - " << SDL_GetError() << "\n";
		return -1;
	}
    return 0;
}
