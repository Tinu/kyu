#pragma once

#include <glew.h>
#include <math.h>

#include "Colour.h"
#include "rt3dObjLoader.h"
#include "ShaderManager.h"
#include "InputHandler.h"
#include "TextureManager.h"

#ifndef RADIANS
#define RADIANS 0.017453293
#endif

#define NEWGAME 0
#define CONTROLS 1
#define ABOUT 2
#define QUIT 3

#define FORWARD 0
#define BACKWARD 1
#define SIDELEFT 2
#define SIDERIGHT 3

#ifndef WIDTH					// width of the screen
#define WIDTH 1280			
#endif

#ifndef HEIGHT					// height of the screen
#define HEIGHT 720
#endif

#ifndef CENTER					// used for centering window position
#define CENTER -1				// improves readability
#endif

#ifndef XCENTRE					// centre of the screen (horizontally)
#define XCENTRE WIDTH/2
#endif

#ifndef YCENTRE					// centre of the screen (vertically)
#define YCENTRE HEIGHT/2
#endif

#define BUTTON_A 0
#define BUTTON_B 1
#define BUTTON_X 2
#define BUTTON_Y 3
#define LB 4
#define RB 5
#define BUTTON_SELECT 6
#define BUTTON_START 7
#define LS 8
#define RS 9
