#include "Light.h"

void Light::init()
	// set the default light colour and send it to shader
{
	reset();
}

void Light::reset()
{
	ambient = Colour(0.38f, 0.37f, 0.3f, 1.0f);
	diffuse = Colour(1.0f, 1.0f, 1.0f, 1.0f);
	specular = Colour(1.0f, 1.0f, 1.0f, 1.0f);
	position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0);
	applyChanges();
	SHADER->updateLight(m_light);
}

void Light::applyChanges()
{
	m_light.ambient[0] = ambient.getRed();
	m_light.ambient[1] = ambient.getGreen();
	m_light.ambient[2] = ambient.getBlue();
	m_light.ambient[3] = ambient.getTransparency();
	m_light.diffuse[0] = diffuse.getRed();
	m_light.diffuse[1] = diffuse.getGreen();
	m_light.diffuse[2] = diffuse.getBlue();
	m_light.diffuse[3] = diffuse.getTransparency();
	m_light.specular[0] = specular.getRed();
	m_light.specular[1] = specular.getGreen();
	m_light.specular[2] = specular.getBlue();
	m_light.specular[3] = specular.getTransparency();
	m_bhasChanged = false;
}

void Light::update()
{
	handleEvents();
	if (m_bhasChanged) 
	{
		applyChanges();					// applies values to light struct
		SHADER->updateLight(m_light);
	}
}

void dark(rt3d::lightStruct &toChange)	// a dark preset for the light
{
	for (int i = 0; i!=3; i++)
	{
		toChange.ambient[i] = toChange.diffuse[i] = toChange.specular[i] = 0.05f;
	}
	toChange.position[0] = 20.0f;
	toChange.position[1] = 0.1f;
	toChange.position[2] = 20.0f;
	SHADER->updateLight(toChange);
}

void Light::handleEvents()
	// events to handle light changes from keyboard
{
	if (PRESSING(SDL_SCANCODE_F5)) { dark(m_light); }

	if (TheInputHandler::Instance()->onKeyDown(SDL_SCANCODE_LSHIFT)||TheInputHandler::Instance()->onKeyDown(SDL_SCANCODE_RSHIFT))
	{
		if (PRESSING(SDL_SCANCODE_KP_1))			{ decreaseSpecularRed();  }
		if (PRESSING(SDL_SCANCODE_KP_2))			{ decreaseSpecularGreen();}
		if (PRESSING(SDL_SCANCODE_KP_3))			{ decreaseSpecularBlue(); }
		if (PRESSING(SDL_SCANCODE_KP_4))			{ decreaseDiffuseRed();   }
		if (PRESSING(SDL_SCANCODE_KP_5))			{ decreaseDiffuseGreen(); }
		if (PRESSING(SDL_SCANCODE_KP_6))			{ decreaseDiffuseBlue();  }
		if (PRESSING(SDL_SCANCODE_KP_7))			{ decreaseAmbientRed();	  }
		if (PRESSING(SDL_SCANCODE_KP_8))			{ decreaseAmbientGreen(); }
		if (PRESSING(SDL_SCANCODE_KP_9))			{ decreaseAmbientBlue();  }
		if (PRESSING(SDL_SCANCODE_KP_DIVIDE))		{ decreaseCoordinateX();  }
		if (PRESSING(SDL_SCANCODE_KP_MULTIPLY))		{ decreaseCoordinateY();  }
		if (PRESSING(SDL_SCANCODE_KP_MINUS))		{ decreaseCoordinateZ();  }	

		if (PRESSING(SDL_SCANCODE_KP_DECIMAL))		
		{ 
			decreaseAmbientBlue(); decreaseAmbientGreen(); decreaseAmbientRed();
			decreaseDiffuseBlue(); decreaseDiffuseGreen(); decreaseDiffuseRed();
			decreaseSpecularBlue(); decreaseSpecularGreen(); decreaseSpecularRed(); 
		}
	}
	else if (ISRELEASED(SDL_SCANCODE_LSHIFT)&&ISRELEASED(SDL_SCANCODE_RSHIFT))
	{
		if (PRESSING(SDL_SCANCODE_KP_1))			{ increaseSpecularRed(); }
		if (PRESSING(SDL_SCANCODE_KP_2))			{ increaseSpecularGreen(); }
		if (PRESSING(SDL_SCANCODE_KP_3))			{ increaseSpecularBlue(); }
		if (PRESSING(SDL_SCANCODE_KP_4))			{ increaseDiffuseRed(); }
		if (PRESSING(SDL_SCANCODE_KP_5))			{ increaseDiffuseGreen(); }
		if (PRESSING(SDL_SCANCODE_KP_6))			{ increaseDiffuseBlue(); }
		if (PRESSING(SDL_SCANCODE_KP_7))			{ increaseAmbientRed(); }
		if (PRESSING(SDL_SCANCODE_KP_8))			{ increaseAmbientGreen(); }
		if (PRESSING(SDL_SCANCODE_KP_9))			{ increaseAmbientBlue(); }
		if (PRESSING(SDL_SCANCODE_KP_DIVIDE))		{ increaseCoordinateX();  }
		if (PRESSING(SDL_SCANCODE_KP_MULTIPLY))		{ increaseCoordinateY();  }
		if (PRESSING(SDL_SCANCODE_KP_MINUS))		{ increaseCoordinateZ();  }

		if (PRESSING(SDL_SCANCODE_RIGHT))			{ increaseCoordinateZ();  }
		if (PRESSING(SDL_SCANCODE_LEFT))			{ decreaseCoordinateZ();  }
		if (PRESSING(SDL_SCANCODE_UP))				{ decreaseCoordinateX();  }
		if (PRESSING(SDL_SCANCODE_DOWN))			{ increaseCoordinateX();  }

		if (PRESSING(SDL_SCANCODE_KP_0))			{ reset(); }

		if (PRESSING(SDL_SCANCODE_KP_DECIMAL))		
		{ 
			increaseAmbientBlue(); increaseAmbientGreen(); increaseAmbientRed();
			increaseDiffuseBlue(); increaseDiffuseGreen(); increaseDiffuseRed();
			increaseSpecularBlue(); increaseSpecularGreen(); increaseSpecularRed(); 
		}
	}
}

void Light::render()
{
	glm::vec4 tmp = ShaderManager::Instance()->m_vStack.top()*position;
	SHADER->updateLightPosition(glm::value_ptr(tmp));
}
