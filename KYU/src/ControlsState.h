#ifndef __ControlsState__
#define __ControlsState__

#include "AbstractGameState.h"

class ControlsState :
	public AbstractGameState
{
private:

	GLuint m_mesh;
	GLuint m_background;

	static const std::string s_menuID;

public:

	ControlsState(void) {}
	~ControlsState(void) {} 

	void update() {}
	void render();
	void handleEvents();

	bool onEnter();
	bool onExit();

	std::string getStateID() const { return s_menuID; }
};

#endif // defined (__ControlsState__)