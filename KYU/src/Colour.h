#pragma once
#include "rt3d.h"

class Colour
{
public:
	GLfloat red;
	GLfloat green;
	GLfloat blue;
	GLfloat transparency;
	GLfloat modifier;
	Colour() { modifier = 0.05f; }
	Colour(GLfloat r, GLfloat g, GLfloat b)
			: red(r), green(g), blue(b), transparency(1.0f) {}
	// set the values for red, green and blue. Transparency is 1.0
	Colour(GLfloat r, GLfloat g, GLfloat b, GLfloat t)
			: red(r), green(g), blue(b), transparency(t) {}
	// set the values for red, green, blue and transparency.
	~Colour(void) {};

	inline GLfloat getRed()						{ return red; };
	inline GLfloat getGreen()					{ return green; };
	inline GLfloat getBlue()					{ return blue; };
	inline GLfloat getTransparency()			{ return transparency; };
	
	inline void setRed	(GLfloat red)			{ this->red = red; };
	inline void setGreen(GLfloat green)			{ this->green = green; };
	inline void setBlue	(GLfloat blue)			{ this->blue = blue; };
	inline void setTransparency(GLfloat alpha)	{ this->transparency = alpha; };
	
	inline void changeRed(GLfloat amount)		{ red += amount; std::cout << "Red: " << red << "\n"; }
	inline void changeGreen(GLfloat amount)		 { green += amount;  std::cout << "Green: " << green<< "\n"; }
	inline void changeBlue(GLfloat amount)		 { blue += amount;std::cout << "Blue: " << blue<< "\n";  }
	inline void changeTransparency(GLfloat amount) { if (transparency < 1.0f) transparency += amount; else transparency = 1.0f; }

};

