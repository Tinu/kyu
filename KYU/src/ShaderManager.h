#ifndef __ShaderManager__
#define __ShaderManager__

#include "rt3d.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <stack>

#ifndef SHADER
#define SHADER ShaderManager::Instance()
#endif

#define MENUSHADER 0
#define GAMESHADER 1

class ShaderManager
{
private:

	static ShaderManager* s_pInstance;
	ShaderManager(void) 
	{ 
		m_bgameShaderInitialised = false;
		m_bmenuShaderInitialised = false;
	};
	bool m_bgameShaderInitialised;
	bool m_bmenuShaderInitialised;
	GLuint gameShader;	
	GLuint menuShader;

public:

	static ShaderManager* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new ShaderManager();
			return s_pInstance;
		}
		
		return s_pInstance;
	}
	
	void updateLight(rt3d::lightStruct &light);
	void updateLight(GLuint &shader, rt3d::lightStruct &light);
	void updateLight(int option, rt3d::lightStruct &light);

	GLuint getShader() { return gameShader; }
	GLuint getIt() { return gameShader; }

	bool isMenuOn() { return m_bmenuShaderInitialised; }
	bool isGameOn() { return m_bgameShaderInitialised; }

	GLuint getMenuShader() { return menuShader; }

	void updateLightPosition(const GLfloat *position);

	void updateMaterial(rt3d::materialStruct &material);
	void updateMaterial(int option, rt3d::materialStruct &material);
	
	void updateValue(char *valueName);
	void updateValue(int option, char *valueName);
	void updateValue(char *valueName, glm::mat4 matrix);
	void updateValue(int option, char *valueName, glm::mat4 matrix);

	void useMenuShader()		{ glUseProgram(menuShader); }
	void useGameShader()		{ glUseProgram(gameShader); }
	void unlink()				{ glUseProgram(0); }
	
	void init(int option);

	std::stack<glm::mat4> m_vStack; 
	void out()				{ m_vStack.pop(); };
	void in(glm::mat4 temp) { m_vStack.push(temp); };
	glm::mat4 top()			{ return m_vStack.top(); }


};

#endif // defined (__ShaderManager__)