#include "ShaderManager.h"

ShaderManager* ShaderManager::s_pInstance = 0;


void ShaderManager::updateLight(rt3d::lightStruct &light)
{
	rt3d::setLight(gameShader,light);
}

void ShaderManager::updateLight(int option, rt3d::lightStruct &light)
{
	if (option == MENUSHADER)
		rt3d::setLight(menuShader,light);
	if (option == GAMESHADER)
		rt3d::setLight(gameShader,light);
}

void ShaderManager::updateLight(GLuint &shader, rt3d::lightStruct &light)
{
	rt3d::setLight(shader,light);
}

void ShaderManager::updateLightPosition(const GLfloat *position)
{
	rt3d::setLightPos(gameShader, position);
}

void ShaderManager::updateMaterial(rt3d::materialStruct &material)
{
	rt3d::setMaterial(gameShader, material);
}

void ShaderManager::updateMaterial(int option, rt3d::materialStruct &material)
{
	if (option == MENUSHADER)
		rt3d::setMaterial(menuShader, material);
	if (option == GAMESHADER)
		rt3d::setMaterial(gameShader, material);
}

void ShaderManager::init(int option)
{
	if (option == MENUSHADER)
	{
	menuShader = rt3d::initShaders("data/shader/phong-tex.vert","data/shader/phong-tex.frag");
	}
	if (option == GAMESHADER)
	{
	gameShader = rt3d::initShaders("data/shader/phong-tex.vert","data/shader/phong-tex.frag");
	}
}

void ShaderManager::updateValue(char *valueName)
{
	rt3d::setUniformMatrix4fv(gameShader, valueName, glm::value_ptr(m_vStack.top()));
}

void ShaderManager::updateValue(int option, char *valueName)
{
	if (option == MENUSHADER)
		rt3d::setUniformMatrix4fv(menuShader, valueName, glm::value_ptr(m_vStack.top()));
	if (option == GAMESHADER)
		rt3d::setUniformMatrix4fv(gameShader, valueName, glm::value_ptr(m_vStack.top()));
}

void ShaderManager::updateValue(char *valueName, glm::mat4 matrix)
{
	rt3d::setUniformMatrix4fv(gameShader, valueName, glm::value_ptr(matrix));
}

void ShaderManager::updateValue(int option, char *valueName, glm::mat4 matrix)
{
	if (option == MENUSHADER)
		rt3d::setUniformMatrix4fv(menuShader, valueName, glm::value_ptr(matrix));
	if (option == GAMESHADER)
		rt3d::setUniformMatrix4fv(gameShader, valueName, glm::value_ptr(matrix));
}