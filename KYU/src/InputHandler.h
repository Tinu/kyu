// This is a reusable Input handler created by Constantin Toader
// after the examples from SDL Game Development book by Shaun Mitchel
// It includes handling input from devices like keyboard, mouse and joystick

#ifndef __InputHandler__
#define __InputHandler__

#ifndef PRESSING
#define PRESSING TheInputHandler::Instance()->isKeyPressed
#endif

#ifndef ISRELEASED
#define ISRELEASED TheInputHandler::Instance()->onKeyUp
#endif

#ifndef JOY
#define JOY InputHandler::Instance()
#endif

#include <SDL.h>
#include <vector>
#include <iostream>
#include "Vector2D.h"
/*
*		This class is a singleton that handles all deveice 
*		input, whether it is from controllers, keyboard or mouse
*/

enum mouseButtons
	// enum used to easily identify which button of a mouse
	// performs an action
{
	LEFT = 0,
	MIDDLE = 1,
	RIGHT = 2,
};

class InputHandler
{
public:

	static InputHandler* Instance()
	{
		if(s_pInstance == 0)
		{
			s_pInstance = new InputHandler();
			return s_pInstance;
		}

		return s_pInstance;
	}
	static float m_joystickSensibility;

	void initialiseJoysticks();
	void update();
	void clean();

	float xValue(int joy, int stick);
	float yValue(int joy, int stick);
	inline bool joysticksInitialised()						{ return m_bJoysticksInitialised; }
	inline bool getButtonState(int joy, int buttonNumber)	{ return m_buttonStates[joy][buttonNumber]; }
	inline bool getMouseButtonState(int buttonNumber)		{ return m_mouseButtonStates[buttonNumber]; }
	inline Vector2D* getMousePosition()						{ return m_mousePosition; }
	inline bool isKeyPressed(SDL_Scancode key) 
	{
		if (SDL_GetKeyboardState(NULL) != 0)
		{
			if (SDL_GetKeyboardState(NULL)[key] == 1) return true;	else return false;
		}
		return false;
	}
	
	// handle keyboard events
	bool onKeyUp(SDL_Scancode key);
	bool onKeyDown(SDL_Scancode key);
	std::vector<std::pair<Vector2D*, Vector2D*>> m_joystickValues;
private:
	static InputHandler* s_pInstance;
	
	InputHandler(void);
	~InputHandler(void) {}

	bool m_bJoysticksInitialised;
	std::vector<SDL_Joystick*> m_joysticks; 
	// will know how many joysticks SDL has access to
	
	// use Vector2D values to set whether a stick has moved up, down, left or right
	std::vector<std::vector<bool>> m_buttonStates;
	// array of boolean values to be used by each controller 
	// first is the joystick id and second is an array of booleans 
	// values for each button on each controller
	std::vector<bool> m_mouseButtonStates;
	// same principle but only one array is needed as there is only 
	// one mouse so no index needed, but the array to hold the booleans
	// for each mouse button
	Vector2D* m_mousePosition;
	Uint8* m_keystate;					
	// obvious 

	// private functions to handle different event types


	// handle mouse vents
	void onMouseMove(SDL_Event& event);
	void onMouseButtonDown(SDL_Event& event);
	void onMouseButtonUp(SDL_Event& event);

	// handle joystick events
	void onJoystickAxisMove(SDL_Event& event);
	void onJoystickButtonDown(SDL_Event& event);
	void onJoystickButtonUp(SDL_Event& event);

};

typedef InputHandler TheInputHandler;

#endif // defined (__InputHandler__)