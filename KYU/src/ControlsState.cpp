#include "ControlsState.h"
#include "Game.h"
#include "MenuState.h"


const std::string ControlsState::s_menuID = "CONTROLS";

void ControlsState::render()
{
	glClearColor(0.0f, 0.0f, 0.0f,0.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, m_background);
	rt3d::drawMesh(m_mesh,6,GL_TRIANGLES);
}

void ControlsState::handleEvents()
{
	if (PRESSING(SDL_SCANCODE_ESCAPE))
	{
		TheGame::Instance()->getStateMachine()->popState();
		SDL_Delay(200);
	}
}

bool ControlsState::onEnter()
{
	if (SHADER->isMenuOn())
		SHADER->useMenuShader();
	else
		SHADER->init(MENUSHADER);
	// set the rectangle that covers all the screen
	rt3d::lightStruct light = 
	{
		{0.9f, 0.9f, 0.9f, 1.0f}, // ambient
		{0.7f, 0.7f, 0.7f, 1.0f}, // diffuse
		{0.8f, 0.8f, 0.8f, 1.0f}, // specular
		{0.5f, 0.5f, 0.5f, 0.0f}  // position
	};
	SHADER->updateLight(MENUSHADER,light);

	rt3d::materialStruct material = 
	{
		{1.0f, 1.0f, 1.0f, 1.0f},	// ambient
		{1.0f, 1.0f, 1.0f, 1.0f},	// diffuse
		{1.0f, 1.0f, 1.0f, 1.0f},	// specular
		2.0f						// shininess
	};
	SHADER->updateMaterial(MENUSHADER,material);

	float backgroundSize = 0.58f;
	float aspectRatio = (float) WIDTH/HEIGHT;
	float backgroundWidth = backgroundSize * aspectRatio;

	GLfloat cubeVerts[] = 
	{
		// first triangle
		-backgroundWidth, -backgroundSize, -1, 
		 backgroundWidth,  backgroundSize, -1,
		 backgroundWidth, -backgroundSize, -1,
		// second triangle	
		-backgroundWidth, -backgroundSize, -1,
		-backgroundWidth,  backgroundSize, -1,
		 backgroundWidth,  backgroundSize, -1,
	};

	GLfloat cubeTexCoords[] = 
	{ 
		0, 1,  1, 0,  1, 1,   
		0, 1,  0, 0,  1, 0 
	};

	// load the background picture to bind to the rectangle
	m_background = TheTextureManager::Instance()->loadImage("data/texture/controlsstate.bmp");
	
	// create the mesh
	m_mesh = rt3d::createMesh(6, cubeVerts, nullptr, cubeVerts, cubeTexCoords);

	SHADER->updateValue(MENUSHADER,"projection", glm::perspective(60.0f,(float) WIDTH/HEIGHT,1.0f,1500.0f));
	SHADER->updateValue(MENUSHADER,"modelview", glm::mat4 (1.0f));		
	
	return true;
}

bool ControlsState::onExit()
{
	return true;
}